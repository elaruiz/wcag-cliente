import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createLogger } from 'redux-logger';
import promise from 'redux-promise-middleware';
import thunk from 'redux-thunk';
import reducer from '../reducers';
import { APP_DEBUG } from "../../constants/constants";

let middlewares = [thunk, promise()];

if(APP_DEBUG) {
    middlewares.push(createLogger());
}

const middleware = applyMiddleware(...middlewares);

export default createStore(reducer, composeWithDevTools(middleware));
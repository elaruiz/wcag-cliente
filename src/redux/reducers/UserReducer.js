const initialState = {
    user: null,
    loading: false,
    error: null
};

export default (state = initialState, action) => {
    switch(action.type) {
        case "USER_LOGOUT":
            return { ...state, loading: false, error: null, user: null };

        //region USER_LOGIN
        case "USER_LOGIN_PENDING":
            return {...state, loading: true, loaded: false};
        case "USER_LOGIN_FULFILLED":
            return {...state, loading: false, loaded: true, error: null};
        case "USER_LOGIN_REJECTED":
            return {...state, loading: false, loaded: false, error: action.payload};
        //endregion

        //region USER_INFO
        case "USER_INFO_FULFILLED":
            return {...state, loading: false, loaded: true, error: null, user: Object.assign({}, action.payload)};
        case "USER_INFO_PENDING":
            return {...state, loading: true, loaded: true};
        case "USER_INFO_REJECTED":
            return {...state, loading: false, loaded: false, error: action.payload};
        //endregion

        default:
            return state;
    }
}
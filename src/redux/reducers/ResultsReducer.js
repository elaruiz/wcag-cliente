const initialState = {
    loading:false,
    error: null,
    data:null
};

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case 'RESET_TEST_RESULTS':
            return { ...state, error:null, loading:false, data:null };
        case 'GET_TEST_PENDING':
            return { ...state, loading: true, error:null, data:null };
        case 'GET_TEST_FULFILLED':
            return { ...state,  error:null, loading:false, data:payload};
        case 'GET_TEST_REJECTED':
            return { ...state,  error: payload, loading:false, data:null };
        default:
            return state;
    }
}
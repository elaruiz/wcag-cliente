import { combineReducers } from "redux";
import { reducer as formReducer } from 'redux-form';
import user from './UserReducer';
import github from './ExampleReducer';
import test from './TestReducer';
import results from './ResultsReducer';

export default combineReducers({
    user,
    github,
    test,
    results,
    form: formReducer
});
const initialState = {
    repos: [],
    loading: false,
    loaded: true,
    error: null,
};

export default (state = initialState, action) => {
    switch(action.type){
        case "GET_USER_REPOS_PENDING":
            return {...state, loading: true, loaded: false, error: null};
        case "GET_USER_REPOS_FULFILLED":
            return {...state, loading: false, loaded: true, error: null, repos: action.payload};
        case "GET_USER_REPOS_REJECTED":
            return {...state, loading: false, loaded: false, error: action.payload};
        default:
            return state;
    }
}
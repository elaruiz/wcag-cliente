const initialState = {
    done: false,
    loading:false,
    error: null,
    result: null
};

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case 'TEST_PENDING':
            return { ...state, done: false, loading: true, error:null, result: null};
        case 'TEST_FULFILLED':
            return { ...state, done: true, error:null, loading:false, result: payload};
        case 'TEST_REJECTED':
            return { ...state, done: false, error: payload, loading:false, result:null};
        case 'RESET_TEST':
            return { ...state, done: false, error:null, loading:false, result: null}
        default:
            return state;
    }
}
import Network from '../../constants/network';
import Cookies from 'js-cookie'
import { API_URL, BASE_URL, CLIENT_ID, CLIENT_SECRET, TOKEN_COOKIE } from '../../constants/constants';

export const getUserInfo = () => {
    return {
        type: "USER_INFO",
        payload: Network.GET(`${API_URL}/user`)
    }
};

export const doLogout = () => {
    Cookies.remove(TOKEN_COOKIE);
    return {
        type: "USER_LOGOUT"
    }
};

export const doLogin = (username, password) => {
    return dispatch => {
        const data = {
            username,
            password,
            client_id: CLIENT_ID,
            client_secret: CLIENT_SECRET,
            grant_type: 'password'
        };

        dispatch({ type: "USER_LOGIN_PENDING" });

        Network.POST(`${BASE_URL}/oauth/token`, data)
            .then(response => {
                Cookies.set(TOKEN_COOKIE, response.access_token);
                dispatch(userIsLoggedIn());
            })
            .catch(error => dispatch({ type: "USER_LOGIN_REJECTED", payload: error }));
    }
};

export const userIsLoggedIn = () => {
    return dispatch => {
        dispatch(getUserInfo())
        //Aquí va cualquier otra acción que se toma cuando el usuario
        //hace login
    }
};
import {API_URL} from "../../constants/constants";
import Network from "../../constants/network";

export const doTest = (data) => {
    return dispatch => {
        dispatch({
            type: 'TEST_PENDING'
        });
        Network.POST(`${API_URL}test`, data)
        .then(( res ) => {
            dispatch({
                type: 'TEST_FULFILLED',
                payload: res
            });
            if (!res.analizada)
            {
              dispatch({
                type: 'GET_TEST_FULFILLED',
                payload: res
            });  
            } 
        })
        .catch(error => dispatch({type: 'TEST_REJECTED', payload: error}))
    };
};

export const resetTest = () => {
    return {
        type: 'RESET_TEST'
    }
};

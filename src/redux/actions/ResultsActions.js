import {API_URL} from "../../constants/constants";
import Network from "../../constants/network";


export const getTest = (id) => {
    return {
        type: 'GET_TEST',
        payload: Network.GET(`${API_URL}last-test/${id}`)
    }
};

export const resetTestResults = () => {
    return {
        type: 'RESET_TEST_RESULTS'
    }
};

import Network from "../../constants/network";

export const getUserRepos = (user) => {
    return {
        type: "GET_USER_REPOS",
        payload: Network.GET(`https://api.github.com/users/${user}/repos`)
    }
};
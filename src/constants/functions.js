export const loadScript = (url) => {
    return new Promise((resolve,reject) => {
        let script = document.createElement('script');
        script.src = url;
        script.addEventListener('load', () => {
            resolve();
        });
        script.addEventListener('error', () => {
            reject();
        });
        document.body.appendChild(script);
    });
};

export const resolveFields = (obj, fields) => {
    let result = obj;
    fields.split(".").forEach(field => {
        if(result !== undefined) {
            result = result[field];
        }
    });
    return result;
};
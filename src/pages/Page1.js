import React, { Component } from "react";
import { getUserRepos } from "../redux/actions/ExampleActions";
import { connect } from "react-redux";

class Page1 extends Component {

    state = {
        githubUser: ''
    };

    render = () => {
        const { repos, loading } = this.props;
        return (
            <div>
                <form onSubmit={this.loadFromNetwork}>
                    Ejemplo para cargar de un API
                    <div className="form-group">
                        <label htmlFor="textField">Github Username</label>
                        <input type="text" className="form-control"
                               id="textField"
                               value={this.state.githubUser}
                               onChange={this.changeValue}/>
                        <button type="submit" className="btn btn-primary">Enviar</button>
                        {
                            loading ? (
                                <p>Cargando...</p>
                            ) : (
                                <ol>
                                    {
                                        repos.map(r => <li key={`repo${r.id}`}>{r.name}</li>)
                                    }
                                </ol>
                            )
                        }
                    </div>
                </form>
            </div>
        )
    };

    changeValue = (event) => {
        this.setState({
            githubUser: event.target.value
        })
    };

    loadFromNetwork = (event) => {
        event.preventDefault();

        this.props.actions.getUserRepos(this.state.githubUser);
    }
}

export default connect(
    state => {
        return {
            repos: state.github.repos,
            loading: state.github.loading
        }
    },
    dispatch => {
        return {
            getUserRepos: (user) => dispatch(getUserRepos(user))
        }
    },
    (stateProps, dispatchProps, ownProps) => Object.assign({}, ownProps, stateProps, { actions: dispatchProps })
)(Page1);
import React from 'react';
import { connect } from "react-redux";
import { Grid, Row, Col } from "react-bootstrap";
import Paper from 'material-ui/Paper';
import AccessibilityForm from "../components/AccessibilityForm";

import '../sass/App.css';

const HomePage = () => {
    return (
        <Grid>
            <Row>
                <Col xs={12}>
                    <Paper className="paper-test" zDepth={1}>
                        <AccessibilityForm/>
                    </Paper>
                </Col>
            </Row>
        </Grid>
    );
};
export default connect(
    state => {
        return {
            user: state.user.user
        }
    }
)(HomePage);
import React, {Component} from 'react';
import queryString from 'query-string';
import ResultsTable from "../components/ResultsTable";
import ResultsMatrix from "../components/ResultsMatrix";
import Chart from "../components/Chart";
import Loading from '../components/Loading';
import {Tabs, Tab} from 'material-ui/Tabs';
import Paper from 'material-ui/Paper';
import { getTest } from "../redux/actions/ResultsActions";
import { connect } from "react-redux";
import moment from 'moment/moment';
import 'moment/locale/es';

import '../sass/App.css';

class Results extends Component {
    state = {
        firstTest : true
    };
    handleChange = (val) => {
        this.setState({firstTest: val});
    };
    componentDidMount = () => {
        const {params} = this.props.match;
        const {id} = params;
        if (this.props.results === null)
        {
           this.props.actions.getTest(id);
           this.handleChange(false)
        }
    };
    render() {
        const {results, loading, error } = this.props;
         if (this.props.results) {
            let protocolo = results.data.analisis.reduce((prev, k) => {
                 return Object.assign(prev, k.analisis.map((l) => { return l.protocolo }))
             }, {} );
         }
        if (loading) 
        return (
            <Loading visible={loading} first = {this.state.firstTest} />
        );
        else
        {
            if (results)
            return (
            <div className={"results"}>
            <p>Resultados para la página: {results.data.pagina.url}</p>
            
            <p>Fecha de analisis: {moment(results.data.pagina.last_analysis).format('DD MMMM YYYY HH:mm:ss')} </p>
            <Tabs>
                <Tab
                label="Resultados">
                    {/*{*/}
                        {/*results.data.analisis.map( (row, index) => (*/}
                            {/*<Paper className="paper-result" zDepth={1} key={index} >*/}
                                {/*<ResultsTable data={row}/>*/}
                            {/*</Paper>) ) */}
                    {/*}  */}
                    <ResultsMatrix results={results}/>
                </Tab>
                <Tab
                label="Resumen">
                    <Chart results = {results.data.analisis}/>
                </Tab>
            </Tabs>
            </div> 
            )
            else 
            return (
                <div>Sin resultados</div>
            )
       } 
    }
}

export default connect(
    state => {
        return {
            results: state.results.data,
            loading: state.results.loading,
            error: state.results.error
        }
    },
    dispatch => {
        return {
            getTest: (id) => dispatch(getTest(id))
        }
    },
    (stateProps, dispatchProps, ownProps) => Object.assign({}, ownProps, stateProps, { actions: dispatchProps })
)(Results);
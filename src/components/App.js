import React from "react";
import { HashRouter, Route, Switch } from "react-router-dom";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';


import Home from "../pages/Home";
import Results from "../pages/Results";

//Cada componente tiene su CSS
//aquí se importa el css que se genera cuando se compila el sass correspondiente
import "../sass/App.css";

const App = () => {
    return (
        <MuiThemeProvider>
            <AppBar title="Accesibilidad Web (WCAG 2.0)" />
            <HashRouter>
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route path="/resultado/:id" component={Results}/> 
                    <Route render={() => <span>404</span>}/>
                </Switch>
            </HashRouter>
        </MuiThemeProvider>
    );
};

export default App;
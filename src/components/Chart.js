import React from 'react';
import Plot from 'react-plotly.js';
import { Divider } from 'material-ui';

var colors = [];
colors['BIEN'] = '#2ca02c';
colors['ADVERTENCIA'] = '#bcbd22';
colors['FALLA'] = '#d62728';
colors['REVISION'] = '#ff7f0e';
colors['IMPOSIBLE'] = '#1f77b4';

export default class Char extends React.Component {
  render() {
    const { results } = this.props;
    let level_data=[]
    let results_level=[]
    let results_guideline=[]
    results.map( res =>{
       let trace = {
         y: res.count_level.map(a => a.total),
         x: res.count_level.map(a => a.id_protocolo__nivel),
         name: res.descripcion,
         type: 'bar'
      }
      level_data.push(trace) 
      
      let trace_res_a = {
        y: [res.count_all['nivel_A_falla'], res.count_all['nivel_A_bien'], res.count_all['nivel_A_revision'], res.count_all['nivel_A_imposible']],
        x: [...new Set(Object.keys(res.count_all).map((x) => x.split("_")[2]))],
        name: `${res.descripcion} - Nivel A`,
        type: 'bar'
      }
      let trace_res_aa = {
        y: [res.count_all['nivel_AA_falla'], res.count_all['nivel_AA_bien'], res.count_all['nivel_AA_revision'], res.count_all['nivel_AA_imposible']],
        x: [...new Set(Object.keys(res.count_all).map((x) => x.split("_")[2]))],
        name: `${res.descripcion} - Nivel AA`,
        type: 'bar'
      }
      let trace_res_aaa = {
        y: [res.count_all['nivel_AAA_falla'], res.count_all['nivel_AAA_bien'], res.count_all['nivel_AAA_revision'], res.count_all['nivel_AAA_imposible']],
        x: [...new Set(Object.keys(res.count_all).map((x) => x.split("_")[2]))],
        name: `${res.descripcion} - Nivel AAA`,
        type: 'bar'
      }
      results_level.push([trace_res_a, trace_res_aa, trace_res_aaa])


      let trace_res_op = {
        y: [res.count_guideline['operable_falla'], res.count_guideline['operable_bien'], res.count_guideline['operable_revision'], res.count_guideline['operable_imposible']],
        x: [...new Set(Object.keys(res.count_guideline).map((x) => x.split("_")[1]))],
        name: `${res.descripcion} - Operable`,
        type: 'bar',
      }
      let trace_res_pe = {
        y: [res.count_guideline['perceptible_falla'], res.count_guideline['perceptible_bien'], res.count_guideline['perceptible_revision'], res.count_guideline['perceptible_imposible']],
        x: [...new Set(Object.keys(res.count_guideline).map((x) => x.split("_")[1]))],
        name: `${res.descripcion} - Perceptible`,
        type: 'bar'
      }
      let trace_res_co = {
        y: [res.count_guideline['comprensible_falla'], res.count_guideline['comprensible_bien'], res.count_guideline['comprensible_revision'], res.count_guideline['comprensible_imposible']],
        x: [...new Set(Object.keys(res.count_guideline).map((x) => x.split("_")[1]))],
        name: `${res.descripcion} - Comprensible`,
        type: 'bar'
      }
      let trace_res_ro = {
        y: [res.count_guideline['robusto_falla'], res.count_guideline['robusto_bien'], res.count_guideline['robusto_revision'], res.count_guideline['robusto_imposible']],
        x: [...new Set(Object.keys(res.count_guideline).map((x) => x.split("_")[1]))],
        name: `${res.descripcion} - Robusto`,
        type: 'bar'
      }
      results_guideline.push([trace_res_op, trace_res_pe, trace_res_co, trace_res_ro])
      }
   )
    return (
      <div style={{textAlign: 'center'}}>
        <div>
          <h2>Cantidad de criterios evaluados</h2>
          {results.map(res => 
          <Plot
            key = {`pie-${res.id}`}
            data = {[{
              values: res.count_criteria.map(a => a.total),
              labels: res.count_criteria.map(a => a.resultado),
              type: 'pie',
                marker : {
                    colors : res.count_criteria.map(a => colors[a.resultado])
                }
            }]}
            layout = {{title:res.descripcion}}

            />) 
            }
          </div>
          <br />
          <div>
            <h2>Cantidad de criterios evaluados por niveles</h2>
            <Plot
              key = {`bar-group`}
              data = {level_data}
              layout = {{barmode: 'group', xaxis: {title: 'Niveles'},
              yaxis: {title: 'Número de criterios'},title:'Gráfico agrupado criterios por nivel'}}
              />
          </div>
          <br />
          <div>
          <h2>Resultados obtenidos por niveles</h2>
          {results_level.map((res, i) => 
          <Plot
            key = {`bar-group-level-${i}`}
            data = {res}
            layout = {{barmode: 'group', xaxis: {title: 'Resultados'},
            yaxis: {title: 'Número de criterios'},title:res.descripcion}}
            />) 
            }
          </div>
          <br />
          <div>
          <h2>Resultados obtenidos por pautas</h2>
          {results_guideline.map((res, i) => 
          <Plot
            key = {`bar-group-guideline-${i}`}
            data = {res}
            layout = {{barmode: 'group', xaxis: {title: 'Resultados'},
            yaxis: {title: 'Número de criterios'},title:res.descripcion}}
            />) 
            }
          </div>
        </div>);
  }

}


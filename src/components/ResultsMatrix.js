import React, {Component} from 'react';
import {
  Table,
  TableBody,
  TableFooter,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

import '../sass/Results.css';
var imgs = [];
imgs['BIEN'] = 'icons/checked.png';
imgs['ADVERTENCIA'] = 'icons/exclamation.png';
imgs['FALLA'] = 'icons/error.png';
imgs['REVISION'] = 'icons/manual.png';
imgs['IMPOSIBLE'] = 'icons/disabled.png';

export default class ResultsMatrix extends Component {
  render() {
    const { results } = this.props;
         let protocolo = results.data.analisis.reduce((prev, k) => {
              return Object.assign(prev, k.analisis.map((l) => { return l.protocolo }))
          }, {} );

          let analizadores = results.data.analisis.map((k) => k.descripcion);
    return (
        <Table
          height={'500px'}
          fixedHeader={true}
          fixedFooter={true}>
          <TableHeader  displaySelectAll={false}
            adjustForCheckbox={false}>
            <TableRow style={{textAlign: 'center'}}>
              <TableHeaderColumn tooltip="Código del criterio de éxito">Código</TableHeaderColumn>
              <TableHeaderColumn tooltip="Título del criterio de éxito">Criterio</TableHeaderColumn>
              if (analizadores){
                analizadores.map((val, index) => (<TableHeaderColumn key= {index} tooltip={val}>{val}</TableHeaderColumn>))
              }
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {
                Object.keys(protocolo).map( (key, index) => (
                  <TableRow key={index} style={{textAlign: 'center'}}>
                    <TableRowColumn>{protocolo[key].codigo}</TableRowColumn>
                    <TableRowColumn>{protocolo[key].criterio}</TableRowColumn>
                    if (analizadores){
                      analizadores.map((val, index) => (
                        <TableRowColumn>
                        <Table selectable={false}>
                        <TableBody  displayRowCheckbox={false}>
                          <TableRow>
                          {results.data.analisis.map((row, i) => (
                              row.analisis.filter( row => (index === i && row.protocolo.codigo === protocolo[key].codigo )).map((r, k) => (
                          <TableRowColumn key={k}>
                            <img className="img-result" src={imgs[r.resultado]} alt={r.resultado} />
                          </TableRowColumn>))
                          ))}
                          </TableRow>
                        </TableBody>
                      </Table>
                      </TableRowColumn>))
                    }
                      
                  </TableRow>
                  )
                )
            }
          </TableBody>
          <TableFooter
            adjustForCheckbox={false}
          >
          <TableRow>
              <TableRowColumn colSpan="5" style={{textAlign: 'center'}}>
               Leyenda
              </TableRowColumn>
            </TableRow>
            <TableRow>
              <TableRowColumn><img className="img-result" src={imgs['BIEN']} alt='BIEN' />No hay error</TableRowColumn>
              <TableRowColumn><img className="img-result" src={imgs['FALLA']} alt='FALLA' />Hay error</TableRowColumn>
              <TableRowColumn><img className="img-result" src={imgs['REVISION']} alt='REVISION' />Requiere revisión manual</TableRowColumn>
              <TableRowColumn><img className="img-result" src={imgs['IMPOSIBLE']} alt='IMPOSIBLE' />No se pudo aplicar pruebas</TableRowColumn>
            </TableRow>
          </TableFooter>
        </Table>
    );
  }
}
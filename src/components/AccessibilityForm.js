import React, {Component} from 'react';
import { Field, reduxForm } from 'redux-form';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { doTest, resetTest } from "../redux/actions/TestActions";
import { connect } from 'react-redux';
import Loading from '../components/Loading';
import Confirmation from '../components/Confirmation';


const renderTextField = ({
                             input,
                             label,
                             placeholder,
                             testError,
                             meta: { touched, error },
                             ...custom
                         }) => (
    <TextField
        hintText={placeholder}
        floatingLabelText={label}
        style={{width: '90%'}}
        errorText={testError}
        {...input}
        {...custom}
    />
);

class AccessibilityForm extends Component {

    componentWillUnmount = () => {
        this.props.resetTest();
    };

    componentWillUnmount = () => {
        this.props.resetTest();
    };
    componentWillReceiveProps = (nextProps) => {
        const { test } = this.props;
        if (nextProps.test.done && !nextProps.test.result.analizada)
        {
            var id = nextProps.test.result.data.pagina.id 
            window.location.href = `/#/resultado/${id}?test=last`;
        }
    };
    handleFormSubmit(formProps) {
        const { reset } = this.props;
        this.props.doTest(formProps);
        reset();
    }

    handleStoredAnalysis = () => {
        const { test } = this.props;
        var id = test.result.data.pagina.id 
        window.location.href = `/#/resultado/${id}?test=last`;
    }

    render() {
        const { handleSubmit, test, reset } = this.props;
        return(
            <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                <div>
                    <Field name="url" component={renderTextField} label="Url a analizar" placeholder="Ej: http://xxxxxxxx" testError={(test.error && test.error.error)}/>
                </div>
                <div>
                    <RaisedButton
                        type="submit"
                        label={"Analizar"}
                        primary={true}
                        disabled={ test.loading }
                        style={{margin: 12}}
                    />
                    <RaisedButton
                        type="button"
                        label={"Limpiar campo"}
                        secondary={true} disabled={ test.loading }
                        onClick={
                            () => {
                                reset();
                                this.props.resetTest();
                            }
                        }
                        style={{margin: 12}}
                    />
                </div>
                <Loading visible={test.loading}/>
                <Confirmation 
                    open={ test.result ? test.result.analizada : false}  
                    created_at={ test.result ? test.result.data.analisis.ultimo_analisis : null } 
                    function1 = {this.handleStoredAnalysis} 
                    function2 = { this.props.resetTest } 
                    option1 = {"Recuperar Análisis"}
                    option2 = {"Realizar nuevo análisis"}/>   
            </form>
        )
    }
}

function mapStateToProps(state) {
    return {
        test: ({...state.test})
    };
}

export default connect(mapStateToProps, { doTest, resetTest })(reduxForm({ form: 'accessibility' })(AccessibilityForm));
import React from 'react';
import Dialog from 'material-ui/Dialog';
import CircularProgress from 'material-ui/CircularProgress';

const Loading = ({visible = false, first}) => {
    return (
        <Dialog
            title="Test de accesibilidad web"
            modal={true}
            open={visible}
            style={{textAlign: 'center'}}>
            <div>
                <CircularProgress size={100} thickness={4} />
                {first ? <p>Estamos analizando esta página por primera vez, este proceso puede demorar...</p> :
                <p>Por favor, espere</p>}
            </div>

        </Dialog>
    );
};

export default Loading;
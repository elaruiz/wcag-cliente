import React, {Component} from 'react';
import {
  Table,
  TableBody,
  TableFooter,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

import '../sass/Results.css';

var imgs = [];
imgs['BIEN'] = 'icons/checked.png';
imgs['ADVERTENCIA'] = 'icons/exclamation.png';
imgs['FALLA'] = 'icons/error.png';
imgs['REVISION'] = 'icons/manual.png';
imgs['IMPOSIBLE'] = 'icons/disabled.png';

export default class Results extends Component {
  render() {
    const { data } = this.props;
    return (
        <Table
          height={'500px'}
          fixedHeader={true}
          fixedFooter={true}
        >
          <TableHeader  displaySelectAll={false}
            adjustForCheckbox={false}>
            <TableRow>
              <TableHeaderColumn colSpan="4" tooltip={data.descripcion} style={{textAlign: 'center'}}>
              {data.descripcion}
              </TableHeaderColumn>
            </TableRow>
            <TableRow style={{textAlign: 'center'}}>
              <TableHeaderColumn tooltip="Código del criterio de éxito">Código</TableHeaderColumn>
              <TableHeaderColumn tooltip="Título del criterio de éxito">Criterio</TableHeaderColumn>
              <TableHeaderColumn tooltip="Nivel">Nivel</TableHeaderColumn>
              <TableHeaderColumn tooltip="Resultado">Resultado</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody
          displayRowCheckbox={false}
          >
            {data.analisis.map( (row, index) => (
              <TableRow key={index} style={{textAlign: 'center'}}>
                <TableRowColumn>{row.protocolo.codigo}</TableRowColumn>
                <TableRowColumn>{row.protocolo.criterio}</TableRowColumn>
                <TableRowColumn>{row.protocolo.nivel}</TableRowColumn>
                <TableRowColumn><img className="img-result" src={imgs[row.resultado]} alt={row.resultado} /></TableRowColumn>
              </TableRow>
              ))}
          </TableBody>
          <TableFooter>
            <TableRow style={{textAlign: 'center'}}>
                <TableRowColumn tooltip="Código del criterio de éxito">Código</TableRowColumn>
                <TableRowColumn tooltip="Título del criterio de éxito">Criterio</TableRowColumn>
                <TableRowColumn tooltip="Nivel">Nivel</TableRowColumn>
                <TableRowColumn tooltip="Resultado">Resultado</TableRowColumn>
            </TableRow>
            <TableRow>
                <TableRowColumn colSpan="4" tooltip={data.descripcion} style={{textAlign: 'center'}}>
                    {data.descripcion}
                </TableRowColumn>
            </TableRow>
            <TableRow>
                <TableRowColumn colSpan="4" tooltip={"Nota"}>
                    {imgs.map((image) => image)}
                </TableRowColumn>
            </TableRow>
          </TableFooter>
        </Table>
    );
  }
}
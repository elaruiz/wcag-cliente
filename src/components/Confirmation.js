import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import moment from 'moment/moment';
import 'moment/locale/es';

/**
 * Dialog with action buttons. The actions are passed in as an array of React objects,
 * in this example [FlatButtons](/#/components/flat-button).
 *
 * You can also close this dialog by clicking outside the dialog, or with the 'Esc' key.
 */
export default class Confirmation extends Component {
  render() {
    const { open, created_at, option1, option2, function1, function2 } = this.props;
    const actions = [
      <FlatButton
        label={option1}
        primary={true}
        onClick={function1}
      />,
      <FlatButton
        label={option2}
        primary={true}
        keyboardFocused={true}
        onClick={function2}
      />,
    ];

    return (
    <Dialog
        title="Análisis WCAG 2.0"
        actions={actions}
        modal={false}
        open={open}
        style={{textAlign: 'center'}}
    >
        Esta página fue analizada previamente
        <br />
        Último análisis el {moment(created_at).format('DD MMMM YYYY HH:mm:ss')}
    </Dialog>
    );
  }
}